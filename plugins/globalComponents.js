import Vue from "vue"
import CButton from "@/components/c-components/CButton"
import CCard from "~/components/c-components/CCard"
import CPageHeader from "~/components/c-components/CHeader"
import CSubtitle from "~/components/c-components/CSubtitle"
import CScrollUpButton from "~/components/c-components/CScrollUpButton"
import CInput from "~/components/c-components/CInput"

// const components = {
//   "c-button": CButton,
//   "c-card": CCard,
//   "c-page-header": CPageHeader,
//   "c-subtitle": CSubtitle,
//   "c-scroll-button": CScrollUpButton
// }

Vue.component("c-button", CButton)
Vue.component("c-input", CInput)
Vue.component("c-card", CCard)
Vue.component("c-page-header", CPageHeader)
Vue.component("c-subtitle", CSubtitle)
Vue.component("c-scroll-button", CScrollUpButton)

// Object.entries(components).forEach(([name, component]) => {
//   Vue.component(name, component)
// })
