import Vue from "vue"
import MenuIcon from "vue-material-design-icons/Menu.vue"
import NuxtIcon from "vue-material-design-icons/Nuxt.vue"
import HeartOutlineIcon from "vue-material-design-icons/HeartOutline.vue"
import CloseIcon from "vue-material-design-icons/Close.vue"
import ArrowUpIcon from "vue-material-design-icons/ArrowUp.vue"
import ArrowLeftIcon from "vue-material-design-icons/ArrowLeft.vue"
import HomeIcon from "vue-material-design-icons/Home.vue"

const components = {
  HomeIcon,
  MenuIcon,
  NuxtIcon,
  HeartOutlineIcon,
  CloseIcon,
  ArrowUpIcon,
  ArrowLeftIcon
}

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
