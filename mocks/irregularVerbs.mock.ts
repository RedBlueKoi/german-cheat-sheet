import IIrregularVerb from "~/types/IIrregularVerb"

const irregularVerbs: IIrregularVerb[] = [
  {
    id: 0,
    infinitive: "beginnen",
    present: "beginnt",
    past: "begann",
    perfect: "hat begonnen",
    translation: ""
  },
  {
    id: 1,
    infinitive: "bleiben",
    present: "bleibt",
    past: "blieb",
    perfect: "ist geblieben",
    translation: ""
  },
  {
    id: 2,
    infinitive: "bringen",
    present: "bringt",
    past: "brachte",
    perfect: "hat gebracht",
    translation: ""
  },
  {
    id: 3,
    infinitive: "denken",
    present: "denkt",
    past: "dachte",
    perfect: "hat gedacht",
    translation: ""
  },
  {
    id: 4,
    infinitive: "dürfen",
    present: "darf",
    past: "durfte",
    perfect: "hat gedurft",
    translation: ""
  },
  {
    id: 5,
    infinitive: "essen",
    present: "isst",
    past: "aß",
    perfect: "hat gegessen",
    translation: ""
  },
  {
    id: 6,
    infinitive: "fahren",
    present: "fährt",
    past: "fuhr",
    perfect: "hat/ist gefahren",
    translation: ""
  },
  {
    id: 7,
    infinitive: "fangen",
    present: "fängt",
    past: "fing",
    perfect: "hat gefangen",
    translation: ""
  },
  {
    id: 8,
    infinitive: "finden",
    present: "findet",
    past: "fand",
    perfect: "hat gefunden",
    translation: ""
  },
  {
    id: 9,
    infinitive: "fliegen",
    present: "fliegt",
    past: "flog",
    perfect: "hat/ist geflogen",
    translation: ""
  },
  {
    id: 10,
    infinitive: "geben",
    present: "gibt",
    past: "gab",
    perfect: "hat gegeben",
    translation: ""
  },
  {
    id: 11,
    infinitive: "gehen",
    present: "geht",
    past: "ging",
    perfect: "ist gegangen",
    translation: ""
  },
  {
    id: 12,
    infinitive: "haben",
    present: "hat",
    past: "hatte",
    perfect: "hat gehabt",
    translation: ""
  },
  {
    id: 13,
    infinitive: "heißen",
    present: "heißt",
    past: "hieß",
    perfect: "hat geheißen",
    translation: ""
  },
  {
    id: 14,
    infinitive: "helfen",
    present: "hilft",
    past: "half",
    perfect: "hat geholfen",
    translation: ""
  },
  {
    id: 15,
    infinitive: "kennen",
    present: "kennt",
    past: "kannte",
    perfect: "hat gekannt",
    translation: ""
  },
  {
    id: 16,
    infinitive: "kommen",
    present: "kommt",
    past: "kam",
    perfect: "ist gekommen",
    translation: ""
  },
  {
    id: 17,
    infinitive: "können",
    present: "kann",
    past: "konnte",
    perfect: "hat gekonnt",
    translation: ""
  },
  {
    id: 18,
    infinitive: "lesen",
    present: "liest",
    past: "las",
    perfect: "hat gelesen",
    translation: ""
  },
  {
    id: 19,
    infinitive: "mögen",
    present: "mag",
    past: "mochte",
    perfect: "hat gemocht",
    translation: ""
  },
  {
    id: 20,
    infinitive: "müssen",
    present: "muss",
    past: "musste",
    perfect: "hat gemusst",
    translation: ""
  },
  {
    id: 21,
    infinitive: "nehmen",
    present: "nimmt",
    past: "nahm",
    perfect: "hat genommen",
    translation: ""
  },
  {
    id: 22,
    infinitive: "rufen",
    present: "ruft",
    past: "rief",
    perfect: "hat gerufen",
    translation: ""
  },
  {
    id: 23,
    infinitive: "schlafen",
    present: "schläft",
    past: "schlief",
    perfect: "hat geschlafen",
    translation: ""
  },
  {
    id: 24,
    infinitive: "schreiben",
    present: "schreibt",
    past: "schrieb",
    perfect: "hat geschrieben",
    translation: ""
  },
  {
    id: 25,
    infinitive: "schwimmen",
    present: "schwimmt",
    past: "schwamm",
    perfect: "hat/ist geschwommen",
    translation: ""
  },
  {
    id: 26,
    infinitive: "sehen",
    present: "sieht",
    past: "sah",
    perfect: "hat gesehen",
    translation: ""
  },
  {
    id: 27,
    infinitive: "sein",
    present: "ist",
    past: "war",
    perfect: "ist gewesen",
    translation: ""
  },
  {
    id: 28,
    infinitive: "singen",
    present: "singt",
    past: "sang",
    perfect: "hat gesungen",
    translation: ""
  },
  {
    id: 29,
    infinitive: "sollen",
    present: "soll",
    past: "sollte",
    perfect: "hat gesollt",
    translation: ""
  },
  {
    id: 30,
    infinitive: "sprechen",
    present: "spricht",
    past: "sprach",
    perfect: "hat gesprochen",
    translation: ""
  },
  {
    id: 31,
    infinitive: "stehen",
    present: "steht",
    past: "stand",
    perfect: "hat gestanden",
    translation: ""
  },
  {
    id: 32,
    infinitive: "treffen",
    present: "trifft",
    past: "traf",
    perfect: "hat getroffen",
    translation: ""
  },
  {
    id: 33,
    infinitive: "trinken",
    present: "trinkt",
    past: "trank",
    perfect: "hat getrunken",
    translation: ""
  },
  {
    id: 34,
    infinitive: "tun",
    present: "tut",
    past: "tat",
    perfect: "hat getan",
    translation: ""
  },
  {
    id: 35,
    infinitive: "wissen",
    present: "weiß",
    past: "wusste",
    perfect: "hat gewusst",
    translation: ""
  },
  {
    id: 36,
    infinitive: "wollen",
    present: "will",
    past: "wollte",
    perfect: "hat gewollt",
    translation: ""
  },
  {
    id: 37,
    infinitive: "backen",
    present: "backt",
    past: "buk",
    perfect: " hat gebacken",
    translation: ""
  },
  {
    id: 38,
    infinitive: "biegen",
    present: "biegt",
    past: "bog",
    perfect: "hat/ist gebogen",
    translation: ""
  },
  {
    id: 39,
    infinitive: "bieten",
    present: "bietet",
    past: "bot",
    perfect: "hat geboten",
    translation: ""
  },
  {
    id: 40,
    infinitive: "bitten",
    present: "bittet",
    past: "bat",
    perfect: "hat gebeten",
    translation: ""
  },
  {
    id: 41,
    infinitive: "braten",
    present: "brät",
    past: "briet",
    perfect: "hat gebraten",
    translation: ""
  },
  {
    id: 42,
    infinitive: "fallen",
    present: "fällt",
    past: "fiel",
    perfect: "ist gefallen",
    translation: ""
  },
  {
    id: 43,
    infinitive: "gefallen",
    present: "gefällt",
    past: "gefiel",
    perfect: "hat gefallen",
    translation: ""
  },
  {
    id: 44,
    infinitive: "gewinnen",
    present: "gewinnt",
    past: "gewann",
    perfect: "hat gewonnen",
    translation: ""
  },
  {
    id: 45,
    infinitive: "laden",
    present: "lädt",
    past: "lud",
    perfect: "hat geladen",
    translation: ""
  },
  {
    id: 46,
    infinitive: "laufen",
    present: "läuft",
    past: "lief",
    perfect: "ist gelaufen",
    translation: ""
  },
  {
    id: 47,
    infinitive: "liegen",
    present: "liegt",
    past: "lag",
    perfect: "hat gelegen",
    translation: ""
  },
  {
    id: 48,
    infinitive: "nennen",
    present: "nennt",
    past: "nannte",
    perfect: "hat genannt",
    translation: ""
  },
  {
    id: 49,
    infinitive: "rennen",
    present: "rennt",
    past: "rannte",
    perfect: "ist gerannt",
    translation: ""
  },
  {
    id: 50,
    infinitive: "riechen",
    present: "riecht",
    past: "roch",
    perfect: "hat gerochen",
    translation: ""
  },
  {
    id: 51,
    infinitive: "schließen",
    present: "schließt",
    past: "schloss",
    perfect: "hat geschlossen",
    translation: ""
  },
  {
    id: 52,
    infinitive: "sitzen",
    present: "sitzt",
    past: "saß",
    perfect: "hat gesessen",
    translation: ""
  },
  {
    id: 53,
    infinitive: "steigen",
    present: "steigt",
    past: "stieg",
    perfect: "ist gestiegen",
    translation: ""
  },
  {
    id: 54,
    infinitive: "sterben",
    present: "stirbt",
    past: "starb",
    perfect: "ist gestorben",
    translation: ""
  },
  {
    id: 55,
    infinitive: "treiben",
    present: "treibt",
    past: "trieb",
    perfect: "hat/ist getrieben",
    translation: ""
  },
  {
    id: 56,
    infinitive: "vergessen",
    present: "vergisst",
    past: "vergaß",
    perfect: "hat vergessen",
    translation: ""
  },
  {
    id: 57,
    infinitive: "verlieren",
    present: "verliert",
    past: "verlor",
    perfect: "hat verloren",
    translation: ""
  },
  {
    id: 58,
    infinitive: "wachsen",
    present: "wächst",
    past: "wuchs",
    perfect: "ist gewachsen",
    translation: ""
  },
  {
    id: 59,
    infinitive: "waschen",
    present: "wäscht",
    past: "wusch",
    perfect: "hat gewaschen",
    translation: ""
  },
  {
    id: 60,
    infinitive: "werden",
    present: "wird",
    past: "wurde",
    perfect: "ist geworden",
    translation: ""
  },
  {
    id: 61,
    infinitive: "werfen",
    present: "wirft",
    past: "warf",
    perfect: "hat geworfen",
    translation: ""
  },
  {
    id: 62,
    infinitive: "ziehen",
    present: "zieht",
    past: "zog",
    perfect: "hat/ist gezogen",
    translation: ""
  },
  {
    id: 63,
    infinitive: "befehlen",
    present: "befiehlt",
    past: "befahl",
    perfect: "hat befohlen",
    translation: ""
  },
  {
    id: 64,
    infinitive: "beißen",
    present: "beißt",
    past: "biss",
    perfect: "hat gebissen",
    translation: ""
  },
  {
    id: 65,
    infinitive: "binden",
    present: "bindet",
    past: "band",
    perfect: "hat gebunden",
    translation: ""
  },
  {
    id: 66,
    infinitive: "brechen",
    present: "bricht",
    past: "brach",
    perfect: "hat/ist gebrochen",
    translation: ""
  },
  {
    id: 67,
    infinitive: "brennen",
    present: "brennt",
    past: "brannte",
    perfect: "hat gebrannt",
    translation: ""
  },
  {
    id: 68,
    infinitive: "empfehlen",
    present: "empfiehlt",
    past: "empfahl",
    perfect: "hat empfohlen",
    translation: ""
  },
  {
    id: 69,
    infinitive: "erschrecken",
    present: "erschrickt",
    past: "erschrak",
    perfect: "ist erschrocken",
    translation: ""
  },
  {
    id: 70,
    infinitive: "fliehen",
    present: "flieht",
    past: "floh",
    perfect: "ist geflohen",
    translation: ""
  },
  {
    id: 71,
    infinitive: "fließen",
    present: "fließt",
    past: "floss",
    perfect: "ist geflossen",
    translation: ""
  },
  {
    id: 72,
    infinitive: "fressen",
    present: "frisst",
    past: "fraß",
    perfect: "hat gefressen",
    translation: ""
  },
  {
    id: 73,
    infinitive: "frieren",
    present: "friert",
    past: "fror",
    perfect: "hat/ist gefroren",
    translation: ""
  },
  {
    id: 74,
    infinitive: "gelingen",
    present: "gelingt",
    past: "gelang",
    perfect: "ist gelungen",
    translation: ""
  },
  {
    id: 75,
    infinitive: "gelten",
    present: "gilt",
    past: "galt",
    perfect: "hat gegolten",
    translation: ""
  },
  {
    id: 76,
    infinitive: "genießen",
    present: "genießt",
    past: "genoss",
    perfect: "hat genossen",
    translation: ""
  },
  {
    id: 77,
    infinitive: "geschehen",
    present: "geschieht",
    past: "geschah",
    perfect: "ist geschehen",
    translation: ""
  },
  {
    id: 78,
    infinitive: "gießen",
    present: "gießt",
    past: "goss",
    perfect: "hat gegossen",
    translation: ""
  },
  {
    id: 79,
    infinitive: "gleichen",
    present: "gleicht",
    past: "glich",
    perfect: "hat geglichen",
    translation: ""
  },
  {
    id: 80,
    infinitive: "graben",
    present: "gräbt",
    past: "grub",
    perfect: "hat gegraben",
    translation: ""
  },
  {
    id: 81,
    infinitive: "greifen",
    present: "greift",
    past: "griff",
    perfect: "hat gegriffen",
    translation: ""
  },
  {
    id: 82,
    infinitive: "halten",
    present: "hält",
    past: "hielt",
    perfect: "hat gehalten",
    translation: ""
  },
  {
    id: 83,
    infinitive: "hängen",
    present: "hängt",
    past: "hing",
    perfect: "hat gehangen",
    translation: ""
  },
  {
    id: 84,
    infinitive: "heben",
    present: "hebt",
    past: "hob",
    perfect: "hat gehoben",
    translation: ""
  },
  {
    id: 85,
    infinitive: "klingen",
    present: "klingt",
    past: "klang",
    perfect: "hat geklungen",
    translation: ""
  },
  {
    id: 86,
    infinitive: "lassen",
    present: "lässt",
    past: "ließ",
    perfect: "hat gelassen",
    translation: ""
  },
  {
    id: 87,
    infinitive: "leiden",
    present: "leidet",
    past: "litt",
    perfect: "hat gelitten",
    translation: ""
  },
  {
    id: 88,
    infinitive: "leihen",
    present: "leiht",
    past: "lieh",
    perfect: "hat geliehen",
    translation: ""
  },
  {
    id: 89,
    infinitive: "lügen",
    present: "lügt",
    past: "log",
    perfect: "hat gelogen",
    translation: ""
  },
  {
    id: 90,
    infinitive: "meiden",
    present: "meidet",
    past: "mied",
    perfect: "hat gemieden",
    translation: ""
  },
  {
    id: 91,
    infinitive: "messen",
    present: "misst",
    past: "maß",
    perfect: "hat gemessen",
    translation: ""
  },
  {
    id: 92,
    infinitive: "raten",
    present: "rät",
    past: "riet",
    perfect: "hat geraten",
    translation: ""
  },
  {
    id: 93,
    infinitive: "reiben",
    present: "reibt",
    past: "rieb",
    perfect: "hat gerieben",
    translation: ""
  },
  {
    id: 94,
    infinitive: "reiten",
    present: "reitet",
    past: "ritt",
    perfect: "hat/ist geritten",
    translation: ""
  },
  {
    id: 95,
    infinitive: "schaffen",
    present: "schafft",
    past: "schuf",
    perfect: "hat geschaffen*",
    translation: ""
  },
  {
    id: 96,
    infinitive: "scheiden",
    present: "scheidet",
    past: "schied",
    perfect: "hat/ist geschieden",
    translation: ""
  },
  {
    id: 97,
    infinitive: "scheinen",
    present: "scheint",
    past: "schien",
    perfect: "hat geschienen",
    translation: ""
  },
  {
    id: 98,
    infinitive: "scheißen",
    present: "scheißt",
    past: "schiss",
    perfect: "hat geschissen",
    translation: ""
  },
  {
    id: 99,
    infinitive: "schieben",
    present: "schiebt",
    past: "schob",
    perfect: "hat geschoben",
    translation: ""
  },
  {
    id: 100,
    infinitive: "schießen",
    present: "schießt",
    past: "schoss",
    perfect: "hat/ist geschossen",
    translation: ""
  },
  {
    id: 101,
    infinitive: "schlagen",
    present: "schlägt",
    past: "schlug",
    perfect: "hat geschlagen",
    translation: ""
  },
  {
    id: 102,
    infinitive: "schmeißen",
    present: "schmeißt",
    past: "schmiss",
    perfect: "hat geschmissen",
    translation: ""
  },
  {
    id: 103,
    infinitive: "schneiden",
    present: "schneidet",
    past: "schnitt",
    perfect: "hat geschnitten",
    translation: ""
  },
  {
    id: 104,
    infinitive: "schreien",
    present: "schreit",
    past: "schrie",
    perfect: "hat geschrien",
    translation: ""
  },
  {
    id: 105,
    infinitive: "senden",
    present: "sendet",
    past: "sandte",
    perfect: "hat gesandt*",
    translation: ""
  },
  {
    id: 106,
    infinitive: "sinken",
    present: "sinkt",
    past: "sank",
    perfect: "ist gesunken",
    translation: ""
  },
  {
    id: 107,
    infinitive: "springen",
    present: "springt",
    past: "sprang",
    perfect: "ist gesprungen",
    translation: ""
  },
  {
    id: 108,
    infinitive: "stehlen",
    present: "stiehlt",
    past: "stahl",
    perfect: "hat gestohlen",
    translation: ""
  },
  {
    id: 109,
    infinitive: "stinken",
    present: "stinkt",
    past: "stank",
    perfect: "hat gestunken",
    translation: ""
  },
  {
    id: 110,
    infinitive: "streiten",
    present: "streitet",
    past: "stritt",
    perfect: "hat gestritten",
    translation: ""
  },
  {
    id: 111,
    infinitive: "tragen",
    present: "trägt",
    past: "trug",
    perfect: "hat getragen",
    translation: ""
  },
  {
    id: 112,
    infinitive: "treten",
    present: "tritt",
    past: "trat",
    perfect: "hat/ist getreten",
    translation: ""
  },
  {
    id: 113,
    infinitive: "trügen",
    present: "trügt",
    past: "trog",
    perfect: "hat getrogen",
    translation: ""
  },
  {
    id: 114,
    infinitive: "verzeihen",
    present: "verzeiht",
    past: "verzieh",
    perfect: "hat verziehen",
    translation: ""
  },
  {
    id: 115,
    infinitive: "weisen",
    present: "weist",
    past: "wies",
    perfect: "hat gewiesen",
    translation: ""
  },
  {
    id: 116,
    infinitive: "werben",
    present: "wirbt",
    past: "warb",
    perfect: "hat geworben",
    translation: ""
  },
  {
    id: 117,
    infinitive: "wiegen",
    present: "wiegt",
    past: "wog",
    perfect: "hat gewogen",
    translation: ""
  },
  {
    id: 118,
    infinitive: "zwingen",
    present: "zwingt",
    past: "zwang",
    perfect: "hat gezwungen",
    translation: ""
  },
  {
    id: 119,
    infinitive: "bergen",
    present: "birgt",
    past: "barg",
    perfect: "hat geborgen",
    translation: ""
  },
  {
    id: 120,
    infinitive: "blasen",
    present: "bläst",
    past: "blies",
    perfect: "hat geblasen",
    translation: ""
  },
  {
    id: 121,
    infinitive: "gebären",
    present: "gebärt",
    past: "gebar",
    perfect: "hat geboren",
    translation: ""
  },
  {
    id: 122,
    infinitive: "pfeifen",
    present: "pfeift",
    past: "pfiff",
    perfect: "hat gepfiffen",
    translation: ""
  },
  {
    id: 123,
    infinitive: "reißen",
    present: "reißt",
    past: "riss",
    perfect: "hat gerissen",
    translation: ""
  },
  {
    id: 124,
    infinitive: "ringen",
    present: "ringt",
    past: "rang",
    perfect: "hat gerungen",
    translation: ""
  },
  {
    id: 125,
    infinitive: "saufen",
    present: "säuft",
    past: "soff",
    perfect: "hat gesoffen",
    translation: ""
  },
  {
    id: 126,
    infinitive: "schmelzen",
    present: "schmilzt",
    past: "schmolz",
    perfect: "hat/ist geschmolzen",
    translation: ""
  },
  {
    id: 127,
    infinitive: "schreiten",
    present: "schreitet",
    past: "schritt",
    perfect: "ist geschritten",
    translation: ""
  },
  {
    id: 128,
    infinitive: "schweigen",
    present: "schweigt",
    past: "schwieg",
    perfect: "hat geschwiegen",
    translation: ""
  },
  {
    id: 129,
    infinitive: "schwellen",
    present: "schwillt",
    past: "schwoll",
    perfect: "ist geschwollen",
    translation: ""
  },
  {
    id: 130,
    infinitive: "schwinden",
    present: "schwindet",
    past: "schwand",
    perfect: "ist geschwunden",
    translation: ""
  },
  {
    id: 131,
    infinitive: "schwören",
    present: "schwört",
    past: "schwor",
    perfect: "hat geschworen",
    translation: ""
  },
  {
    id: 132,
    infinitive: "spinnen",
    present: "spinnt",
    past: "spann",
    perfect: "hat gesponnen",
    translation: ""
  },
  {
    id: 133,
    infinitive: "stechen",
    present: "sticht",
    past: "stach",
    perfect: "hat gestochen",
    translation: ""
  },
  {
    id: 134,
    infinitive: "stoßen",
    present: "stößt",
    past: "stieß",
    perfect: "hat/ist gestoßen",
    translation: ""
  },
  {
    id: 135,
    infinitive: "streichen",
    present: "streicht",
    past: "strich",
    perfect: "hat/ist gestrichen",
    translation: ""
  },
  {
    id: 136,
    infinitive: "verderben",
    present: "verdirbt",
    past: "verdarb",
    perfect: "hat/ist verdorben",
    translation: ""
  },
  {
    id: 137,
    infinitive: "weichen",
    present: "weicht",
    past: "wich",
    perfect: "ist gewichen",
    translation: ""
  },
  {
    id: 138,
    infinitive: "wenden",
    present: "wendet",
    past: "wandte",
    perfect: "hat gewandt",
    translation: ""
  },
  {
    id: 139,
    infinitive: "winden",
    present: "windet",
    past: "wand",
    perfect: "hat gewunden",
    translation: ""
  },
  {
    id: 140,
    infinitive: "bewegen",
    present: "bewegt",
    past: "bewog",
    perfect: "hat bewogen",
    translation: ""
  },
  {
    id: 141,
    infinitive: "dringen",
    present: "dringt",
    past: "drang",
    perfect: "hat/ist gedrungen",
    translation: ""
  },
  {
    id: 142,
    infinitive: "erlöschen",
    present: "erlischt",
    past: "erlosch",
    perfect: "ist erloschen",
    translation: ""
  },
  {
    id: 143,
    infinitive: "fechten",
    present: "ficht",
    past: "focht",
    perfect: "hat gefochten",
    translation: ""
  },
  {
    id: 144,
    infinitive: "gedeihen",
    present: "gedeiht",
    past: "gedieh",
    perfect: "ist gediehen",
    translation: ""
  },
  {
    id: 145,
    infinitive: "genesen",
    present: "genest",
    past: "genas",
    perfect: "ist genesen",
    translation: ""
  },
  {
    id: 146,
    infinitive: "gleiten",
    present: "gleitet",
    past: "glitt",
    perfect: "ist geglitten",
    translation: ""
  },
  {
    id: 147,
    infinitive: "klimmen",
    present: "klimmt",
    past: "klomm",
    perfect: "ist geklommen",
    translation: ""
  },
  {
    id: 148,
    infinitive: "kneifen",
    present: "kneift",
    past: "kniff",
    perfect: "hat gekniffen",
    translation: ""
  },
  {
    id: 149,
    infinitive: "kriechen",
    present: "kriecht",
    past: "kroch",
    perfect: "ist gekrochen",
    translation: ""
  },
  {
    id: 150,
    infinitive: "melken",
    present: "melkt",
    past: "molk",
    perfect: "hat gemolken",
    translation: ""
  },
  {
    id: 151,
    infinitive: "preisen",
    present: "preist",
    past: "pries",
    perfect: "hat gepriesen",
    translation: ""
  },
  {
    id: 152,
    infinitive: "quellen",
    present: "quillt",
    past: "quoll",
    perfect: "ist gequollen",
    translation: ""
  },
  {
    id: 153,
    infinitive: "rinnen",
    present: "rinnt",
    past: "rann",
    perfect: "ist geronnen",
    translation: ""
  },
  {
    id: 154,
    infinitive: "saugen",
    present: "saugt",
    past: "sog",
    perfect: "hat gesogen",
    translation: ""
  },
  {
    id: 155,
    infinitive: "schleichen",
    present: "schleicht",
    past: "schlich",
    perfect: "ist geschlichen",
    translation: ""
  },
  {
    id: 156,
    infinitive: "schleifen",
    present: "schleift",
    past: "schliff",
    perfect: "hat geschliffen",
    translation: ""
  },
  {
    id: 157,
    infinitive: "schlingen",
    present: "schlingt",
    past: "schlang",
    perfect: "hat geschlungen",
    translation: ""
  },
  {
    id: 158,
    infinitive: "schwingen",
    present: "schwingt",
    past: "schwang",
    perfect: "hat/ist geschwungen",
    translation: ""
  },
  {
    id: 159,
    infinitive: "sinnen",
    present: "sinnt",
    past: "sann",
    perfect: "hat gesonnen",
    translation: ""
  },
  {
    id: 160,
    infinitive: "sprießen",
    present: "sprießt",
    past: "spross",
    perfect: "ist gesprossen",
    translation: ""
  },
  {
    id: 161,
    infinitive: "wringen",
    present: "wringt",
    past: "wrang",
    perfect: "hat gewrungen",
    translation: ""
  },
  {
    id: 162,
    infinitive: "bersten",
    present: "birst",
    past: "barst",
    perfect: "ist geborsten",
    translation: ""
  },
  {
    id: 163,
    infinitive: "bleichen",
    present: "bleicht",
    past: "blich",
    perfect: "ist geblichen",
    translation: ""
  },
  {
    id: 164,
    infinitive: "dreschen",
    present: "drischt",
    past: "drosch",
    perfect: "hat gedroschen",
    translation: ""
  },
  {
    id: 165,
    infinitive: "flechten",
    present: "flicht",
    past: "flocht",
    perfect: "hat geflochten",
    translation: ""
  },
  {
    id: 166,
    infinitive: "glimmen",
    present: "glimmt",
    past: "glomm",
    perfect: "hat geglommen",
    translation: ""
  },
  {
    id: 167,
    infinitive: "hauen",
    present: "haut",
    past: "hieb",
    perfect: "hat gehauen",
    translation: ""
  },
  {
    id: 168,
    infinitive: "schelten",
    present: "schilt",
    past: "schalt",
    perfect: "hat gescholten",
    translation: ""
  },
  {
    id: 169,
    infinitive: "scheren",
    present: "schert",
    past: "schor",
    perfect: "hat geschoren",
    translation: ""
  },
  {
    id: 170,
    infinitive: "speien",
    present: "speit",
    past: "spie",
    perfect: "hat gespien",
    translation: ""
  },
  {
    id: 171,
    infinitive: "stieben",
    present: "stiebt",
    past: "stob",
    perfect: "hat/ist gestoben",
    translation: ""
  },
  {
    id: 172,
    infinitive: "verdrießen",
    present: "verdrießt",
    past: "verdross",
    perfect: "hat verdrossen",
    translation: ""
  },
  {
    id: 173,
    infinitive: "weben",
    present: "webt",
    past: "wob",
    perfect: "hat gewoben",
    translation: ""
  }
]

export default irregularVerbs
