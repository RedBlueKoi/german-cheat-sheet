import IUsefulVerb from "~/types/IUsefulVerb"
import ECase from "~/types/ECase"

export const usefulVerbs: IUsefulVerb[] = [
  {
    id: 0,
    verb: "ab * hängen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 1,
    verb: "achten",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 2,
    verb: "etwas ändern",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 3,
    verb: "an * fangen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 4,
    verb: "an * kommen",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 5,
    verb: "antworten",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 6,
    verb: "arbeiten",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 7,
    verb: "sich ärgern",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 8,
    verb: "auf * hören",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 9,
    verb: "auf * passen",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 10,
    verb: "sich auf * regen",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 11,
    verb: "aus * geben",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 12,
    verb: "sich bedanken",
    proposition: "bei",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 13,
    verb: "sich bedanken",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 14,
    verb: "beginnen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 15,
    verb: "sich beklagen",
    proposition: "bei",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 16,
    verb: "sich beklagen",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 17,
    verb: "sich bemühen",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 18,
    verb: "berichten",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 19,
    verb: "sich beschäftigen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 20,
    verb: "sich beschweren",
    proposition: "bei",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 21,
    verb: "sich beschweren",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 22,
    verb: "bestehen",
    proposition: "aus",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 23,
    verb: "sich bewerben",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 24,
    verb: "jdn bitten",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 25,
    verb: "jdm danken",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 26,
    verb: "denken",
    proposition: "an",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 27,
    verb: "sich eignen",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 28,
    verb: "jdn ein * laden",
    proposition: "zu",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 29,
    verb: "sich engagieren",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 30,
    verb: "sich entscheiden",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 31,
    verb: "sich entschließen",
    proposition: "zu",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 32,
    verb: "sich entschuldigen",
    proposition: "bei",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 33,
    verb: "sich entschuldigen",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 34,
    verb: "sich erholen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 35,
    verb: "sich erinnern",
    proposition: "an",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 36,
    verb: "erkennen",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 37,
    verb: "sich erkundigen",
    proposition: "bei",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 38,
    verb: "sich erkundigen",
    proposition: "nach",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 39,
    verb: "erzählen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 40,
    verb: "fliehen",
    proposition: "vor",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 41,
    verb: "fragen",
    proposition: "nach",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 42,
    verb: "sich freuen",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 43,
    verb: "sich freuen",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 44,
    verb: "sich fürchten",
    proposition: "vor",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 45,
    verb: "gehören",
    proposition: "zu",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 46,
    verb: "es geht",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 47,
    verb: "sich gewöhnen",
    proposition: "an",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 48,
    verb: "glauben",
    proposition: "an",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 49,
    verb: "jdm gratulieren",
    proposition: "zu",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 50,
    verb: "handeln",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 51,
    verb: "es handelt sich",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 52,
    verb: "halten",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 53,
    verb: "halten",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 54,
    verb: "jdm helfen",
    proposition: "bei",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 55,
    verb: "hören",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 56,
    verb: "hoffen",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 57,
    verb: "(sich) informieren",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 58,
    verb: "sich interessieren",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 59,
    verb: "kämpfen",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 60,
    verb: "kämpfen",
    proposition: "gegen",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 61,
    verb: "kämpfen",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 62,
    verb: "sich kümmern",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 63,
    verb: "lachen",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 64,
    verb: "leiden",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 65,
    verb: "leiden",
    proposition: "unter",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 66,
    verb: "es liegt",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 67,
    verb: "nach * denken",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 68,
    verb: "passen",
    proposition: "zu",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 69,
    verb: "protestieren",
    proposition: "gegen",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 70,
    verb: "rechnen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 71,
    verb: "riechen",
    proposition: "nach",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 72,
    verb: "schmecken",
    proposition: "nach",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 73,
    verb: "(etwas) schreiben",
    proposition: "an",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 74,
    verb: "sich sehnen",
    proposition: "nach",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 75,
    verb: "sorgen",
    proposition: "für",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 76,
    verb: "sich sorgen",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 77,
    verb: "sprechen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 78,
    verb: "sprechen",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 79,
    verb: "sprechen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 80,
    verb: "sterben",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 81,
    verb: "sich streiten",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 82,
    verb: "(sich) streiten",
    proposition: "um",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 83,
    verb: "suchen",
    proposition: "nach",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 84,
    verb: "teil * nehmen",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 85,
    verb: "telefonieren",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 86,
    verb: "träumen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 87,
    verb: "trennen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 88,
    verb: "jdn überreden",
    proposition: "zu",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 89,
    verb: "jdn überzeugen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 90,
    verb: "sich unterhalten",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 91,
    verb: "sich unterscheiden",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 92,
    verb: "sich verabreden",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 93,
    verb: "vergleichen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 94,
    verb: "sich verlassen",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 95,
    verb: "sich verlieben",
    proposition: "in",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 96,
    verb: "sich vertragen",
    proposition: "mit",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 97,
    verb: "sich vor * bereiten",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 98,
    verb: "jdn warnen",
    proposition: "vor",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 99,
    verb: "warten",
    proposition: "auf",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 100,
    verb: "sich wenden",
    proposition: "an",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 101,
    verb: "wissen",
    proposition: "von",
    case: ECase.Dat,
    translation: ""
  },
  {
    id: 102,
    verb: "sich wundern",
    proposition: "über",
    case: ECase.Akk,
    translation: ""
  },
  {
    id: 103,
    verb: "zweifeln",
    proposition: "an",
    case: ECase.Dat,
    translation: ""
  }
]
