import IDeclination from "~/types/IDeclination"

export const declination1: IDeclination[] = [
  {
    gender: "Masculine",
    nom: "ein -er",
    dat: "einem -en",
    acc: "einen -en"
  },
  {
    gender: "Feminine",
    nom: "eine -e",
    dat: "einer -en",
    acc: "eine -e"
  },
  {
    gender: "Neutral",
    nom: "ein -es",
    dat: "einem -en",
    acc: "ein -es"
  },
  {
    gender: "Plural",
    nom: "-e / keine -en",
    dat: "(keinen) -en",
    acc: "-e / keine -e"
  }
]

export const declination2: IDeclination[] = [
  {
    gender: "Masculine",
    nom: "der -e",
    dat: "dem -en",
    acc: "den -en"
  },
  {
    gender: "Feminine",
    nom: "die -e",
    dat: "der -en",
    acc: "die -e"
  },
  {
    gender: "Neutral",
    nom: "das -e",
    dat: "dem -en",
    acc: "das -e"
  },
  {
    gender: "Plural",
    nom: "die -en",
    dat: "den -en",
    acc: "die -en"
  }
]

export const declination3: IDeclination[] = [
  {
    gender: "Masculine",
    nom: "-er",
    dat: "-em",
    acc: "-en"
  },
  {
    gender: "Feminine",
    nom: "-e",
    dat: "-er",
    acc: "-e"
  },
  {
    gender: "Neutral",
    nom: "-es",
    dat: "-em",
    acc: "-es"
  },
  {
    gender: "Plural",
    nom: "-e",
    dat: "-en -n",
    acc: "-e"
  }
]
