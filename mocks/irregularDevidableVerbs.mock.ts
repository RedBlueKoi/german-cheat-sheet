import IIrregularVerb from "~/types/IIrregularVerb"

const irregularDividableVerbs: IIrregularVerb[] = [
  {
    id: 1,
    infinitive: "abfahren",
    present: "fährt ab",
    past: "fuhr ab",
    perfect: "ist abgefahren",
    translation: ""
  },
  {
    id: 2,
    infinitive: "abfliegen",
    present: "fliegt ab",
    past: "flog ab",
    perfect: "ist abgeflogen",
    translation: ""
  },
  {
    id: 3,
    infinitive: "abgeben",
    present: "gibt ab",
    past: "gab ab",
    perfect: "hat abgegeben",
    translation: ""
  },
  {
    id: 4,
    infinitive: "abschließen",
    present: "schließt ab",
    past: "schloss ab",
    perfect: "hat abgeschlossen",
    translation: ""
  },
  {
    id: 5,
    infinitive: "anbieten",
    present: "bietet an",
    past: "bot an",
    perfect: "hat angeboten",
    translation: ""
  },
  {
    id: 6,
    infinitive: "anfangen",
    present: "fängt an",
    past: "fing an",
    perfect: "hat angefangen",
    translation: ""
  },
  {
    id: 7,
    infinitive: "ankommen",
    present: "kommt an",
    past: "kam an",
    perfect: "ist angekommen",
    translation: ""
  },
  {
    id: 8,
    infinitive: "anrufen",
    present: "ruft an",
    past: "rief an",
    perfect: "hat angerufen",
    translation: ""
  },
  {
    id: 9,
    infinitive: "ansehen",
    present: "sieht an",
    past: "sah an",
    perfect: "hat angesehen",
    translation: ""
  },
  {
    id: 10,
    infinitive: "anziehen",
    present: "ziehen an",
    past: "zog an",
    perfect: "hat angezogen",
    translation: ""
  }
]

export default irregularDividableVerbs
