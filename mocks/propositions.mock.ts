import IProposition from "~/types/IProposition"
import ECase from "~/types/ECase"

const onlyDat: IProposition[] = [
  {
    name: "bei",
    caseAfter: [ECase.Dat]
  },
  {
    name: "zu",
    caseAfter: [ECase.Dat]
  },
  {
    name: "aus",
    caseAfter: [ECase.Dat]
  },
  {
    name: "mit",
    caseAfter: [ECase.Dat]
  },
  {
    name: "außer",
    caseAfter: [ECase.Dat]
  },
  {
    name: "seit",
    caseAfter: [ECase.Dat]
  },
  {
    name: "von",
    caseAfter: [ECase.Dat]
  },
  {
    name: "nach",
    caseAfter: [ECase.Dat]
  },
  {
    name: "ab",
    caseAfter: [ECase.Dat]
  },
  {
    name: "gegenüber",
    caseAfter: [ECase.Dat]
  }
]

const onlyAkk: IProposition[] = [
  {
    name: "durch",
    caseAfter: [ECase.Akk]
  },
  {
    name: "um",
    caseAfter: [ECase.Akk]
  },
  {
    name: "bis",
    caseAfter: [ECase.Akk]
  },
  {
    name: "gegen",
    caseAfter: [ECase.Akk]
  },
  {
    name: "für",
    caseAfter: [ECase.Akk]
  },
  {
    name: "ohne",
    caseAfter: [ECase.Akk]
  }
]
const wechsel: IProposition[] = [
  {
    name: "in",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "an",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "auf",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "über",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "neben",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "unter",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "hinter",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "zwischen",
    caseAfter: [ECase.Dat, ECase.Akk]
  },
  {
    name: "vor",
    caseAfter: [ECase.Dat, ECase.Akk]
  }
]

const propositionsMock: IProposition[] = [...onlyDat, ...onlyAkk, ...wechsel]

export { onlyAkk, onlyDat, wechsel }
export default propositionsMock
