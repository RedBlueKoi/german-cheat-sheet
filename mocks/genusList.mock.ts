import IGenus from "~/types/IGenus"

const masculine: IGenus = {
  name: "Maskuline",
  article: "Der",
  translation: "Masculine"
}

const feminine: IGenus = {
  name: "Feminine",
  article: "Die",
  translation: "Feminine"
}
const neuter: IGenus = {
  name: "Neutrale",
  article: "Das",
  translation: "Neuter"
}

const genusList: IGenus[] = [masculine, feminine, neuter]

export { masculine, feminine, neuter }
export default genusList
