import INoun from "~/types/INoun"
import { feminine, masculine, neuter } from "~/mocks/genusList.mock"

const nounList: INoun[] = [
  {
    id: 0,
    genus: feminine,
    noun: "Mutter",
    translation: "mother",
    plural: ""
  },
  {
    id: 1,
    genus: masculine,
    noun: "Vater",
    translation: "father",
    plural: ""
  },
  {
    id: 2,
    genus: feminine,
    noun: "Schwester",
    translation: "sister",
    plural: ""
  },
  {
    id: 3,
    genus: masculine,
    noun: "Bruder",
    translation: "brother",
    plural: ""
  },
  {
    id: 4,
    genus: neuter,
    noun: "Kind",
    translation: "child",
    plural: "Kinder"
  },
  {
    id: 5,
    genus: feminine,
    noun: "Tante",
    translation: "aunt",
    plural: ""
  },
  {
    id: 5,
    genus: masculine,
    noun: "Onkel",
    translation: "uncle",
    plural: ""
  },
  {
    id: 6,
    genus: feminine,
    noun: "Groβmutter",
    translation: "grandmother",
    plural: ""
  },
  {
    id: 7,
    genus: masculine,
    noun: "Groβvater",
    translation: "grandfather",
    plural: ""
  },
  {
    id: 8,
    genus: feminine,
    noun: "Cousine",
    translation: "female cousin",
    plural: ""
  },
  {
    id: 9,
    genus: masculine,
    noun: "Cousin",
    translation: "male cousin",
    plural: ""
  },
  {
    id: 10,
    genus: masculine,
    noun: "Freund",
    translation: "boyfriend",
    plural: ""
  },
  {
    id: 11,
    genus: feminine,
    noun: "Freundin",
    translation: "girlfriend",
    plural: ""
  },
  {
    id: 12,
    genus: masculine,
    noun: "Mann",
    translation: "husband",
    plural: ""
  },
  {
    id: 13,
    genus: feminine,
    noun: "Frau",
    translation: "wife",
    plural: ""
  },
  {
    id: 14,
    genus: masculine,
    noun: "Kollege",
    translation: "male colleague",
    plural: ""
  },
  {
    id: 15,
    genus: feminine,
    noun: "Kollegin",
    translation: "female colleague",
    plural: ""
  },
  {
    id: 16,
    genus: masculine,
    noun: "Partner",
    translation: "male partner",
    plural: ""
  },
  {
    id: 17,
    genus: feminine,
    noun: "Partnerin",
    translation: "female partner",
    plural: ""
  },
  {
    id: 18,
    genus: feminine,
    noun: "Lampe",
    translation: "Lamp",
    plural: "Lampen"
  },
  {
    id: 19,
    genus: neuter,
    noun: "Waage",
    translation: "Scale",
    plural: "Waagen"
  },
  {
    id: 20,
    genus: neuter,
    noun: "Haus",
    translation: "house",
    plural: ""
  },
  {
    id: 21,
    genus: neuter,
    noun: "Bett",
    translation: "bed",
    plural: ""
  },
  {
    id: 22,
    genus: masculine,
    noun: "Tisch",
    translation: "table",
    plural: ""
  },
  {
    id: 23,
    genus: feminine,
    noun: "Tür",
    translation: "door",
    plural: ""
  },
  {
    id: 24,
    genus: neuter,
    noun: "Kissen",
    translation: "pillow/cushion",
    plural: ""
  },
  {
    id: 25,
    genus: neuter,
    noun: "Fenster",
    translation: "window",
    plural: ""
  },
  {
    id: 26,
    genus: feminine,
    noun: "Wand",
    translation: "wall",
    plural: ""
  },
  {
    id: 27,
    genus: masculine,
    noun: "Boden",
    translation: "floor",
    plural: ""
  },
  {
    id: 28,
    genus: neuter,
    noun: "Schlafzimmer",
    translation: "bedroom",
    plural: ""
  },
  {
    id: 29,
    genus: neuter,
    noun: "Badezimmer",
    translation: "bathroom",
    plural: ""
  },
  {
    id: 30,
    genus: feminine,
    noun: "Küche",
    translation: "kitchen",
    plural: ""
  },
  {
    id: 31,
    genus: feminine,
    noun: "Wohnung",
    translation: "living room",
    plural: ""
  },
  {
    id: 32,
    genus: masculine,
    noun: "Keller",
    translation: "basement",
    plural: ""
  },
  {
    id: 33,
    genus: feminine,
    noun: "Couch",
    translation: "couch",
    plural: ""
  },
  {
    id: 34,
    genus: masculine,
    noun: "Stuhl",
    translation: "chair",
    plural: ""
  },
  {
    id: 35,
    genus: neuter,
    noun: "Waschbecken",
    translation: "sink",
    plural: ""
  },
  {
    id: 36,
    genus: feminine,
    noun: "Toilette",
    translation: "toilet",
    plural: ""
  },
  {
    id: 37,
    genus: feminine,
    noun: "Badewanne",
    translation: "bathtub",
    plural: ""
  },
  {
    id: 38,
    genus: feminine,
    noun: "Dusche",
    translation: "shower",
    plural: ""
  },
  {
    id: 39,
    genus: feminine,
    noun: "Sonne",
    translation: "sun",
    plural: "Sonnen"
  },
  {
    id: 40,
    genus: masculine,
    noun: "Müll",
    translation: "trash",
    plural: ""
  },
  {
    id: 41,
    genus: masculine,
    noun: "Kühlschrank",
    translation: "refrigerator",
    plural: ""
  },
  {
    id: 42,
    genus: masculine,
    noun: "Herd",
    translation: "stove",
    plural: ""
  },
  {
    id: 43,
    genus: masculine,
    noun: "Mikrowellenherd",
    translation: "microwave",
    plural: ""
  },
  {
    id: 44,
    genus: feminine,
    noun: "Geschirrspülmaschine",
    translation: "dishwasher",
    plural: ""
  },
  {
    id: 45,
    genus: neuter,
    noun: "Kabinett",
    translation: "cabinet",
    plural: ""
  },
  {
    id: 46,
    genus: neuter,
    noun: "Auto",
    translation: "car",
    plural: ""
  },
  {
    id: 47,
    genus: masculine,
    noun: "Lustkraftwagen (LKW)",
    translation: "truck",
    plural: ""
  },
  {
    id: 48,
    genus: masculine,
    noun: "Bus",
    translation: "bus",
    plural: ""
  },
  {
    id: 49,
    genus: neuter,
    noun: "Flugzeug",
    translation: "plane",
    plural: ""
  },
  {
    id: 50,
    genus: masculine,
    noun: "Zug",
    translation: "train",
    plural: ""
  },
  {
    id: 51,
    genus: neuter,
    noun: "Boot",
    translation: "boat",
    plural: ""
  },
  {
    id: 52,
    genus: neuter,
    noun: "Taxi",
    translation: "taxi",
    plural: ""
  },
  {
    id: 53,
    genus: masculine,
    noun: "Schulbus",
    translation: "school bus",
    plural: ""
  },
  {
    id: 54,
    genus: neuter,
    noun: "Ticket",
    translation: "ticket",
    plural: ""
  },
  {
    id: 55,
    genus: masculine,
    noun: "Pass",
    translation: "pass",
    plural: ""
  },
  {
    id: 56,
    genus: masculine,
    noun: "Sattelzug",
    translation: "semi truck",
    plural: ""
  },
  {
    id: 57,
    genus: feminine,
    noun: "Stadt",
    translation: "city",
    plural: ""
  },
  {
    id: 58,
    genus: neuter,
    noun: "Land",
    translation: "country",
    plural: ""
  },
  {
    id: 59,
    genus: masculine,
    noun: "Berg",
    translation: "mountain",
    plural: ""
  },
  {
    id: 60,
    genus: feminine,
    noun: "Ebenen",
    translation: "plains",
    plural: ""
  },
  {
    id: 61,
    genus: feminine,
    noun: "Wüste",
    translation: "desert",
    plural: ""
  },
  {
    id: 62,
    genus: feminine,
    noun: "Schule",
    translation: "school",
    plural: ""
  },
  {
    id: 63,
    genus: feminine,
    noun: "Arbeit",
    translation: "work",
    plural: ""
  },
  {
    id: 64,
    genus: neuter,
    noun: "Heimatland",
    translation: "homeland",
    plural: ""
  },
  {
    id: 65,
    genus: masculine,
    noun: "Pilot",
    translation: "pilot",
    plural: ""
  },
  {
    id: 66,
    genus: masculine,
    noun: "Doktor",
    translation: "doctor",
    plural: ""
  },
  {
    id: 67,
    genus: masculine,
    noun: "Zahnarzt",
    translation: "dentist",
    plural: ""
  },
  {
    id: 68,
    genus: masculine,
    noun: "Bibliothekar",
    translation: "librarian",
    plural: ""
  },
  {
    id: 69,
    genus: masculine,
    noun: "Friseur",
    translation: "hairdresser",
    plural: ""
  },
  {
    id: 70,
    genus: masculine,
    noun: "Rechtsanwalt",
    translation: "lawyer",
    plural: ""
  },
  {
    id: 71,
    genus: masculine,
    noun: "Verkäufer",
    translation: "salesman/woman",
    plural: ""
  },
  {
    id: 72,
    genus: masculine,
    noun: "Busfahrer",
    translation: "bus driver",
    plural: ""
  },
  {
    id: 73,
    genus: masculine,
    noun: "Lehrer",
    translation: "teacher",
    plural: ""
  },
  {
    id: 74,
    genus: masculine,
    noun: "Professor",
    translation: "professor",
    plural: ""
  },
  {
    id: 75,
    genus: masculine,
    noun: "Assistent",
    translation: "assistant",
    plural: ""
  },
  {
    id: 76,
    genus: masculine,
    noun: "Börsenmakler",
    translation: "stock broker",
    plural: ""
  },
  {
    id: 77,
    genus: masculine,
    noun: "Vermarkter",
    translation: "marketer",
    plural: ""
  },
  {
    id: 78,
    genus: masculine,
    noun: "Versicherungsagent",
    translation: "insurance agent",
    plural: ""
  },
  {
    id: 79,
    genus: masculine,
    noun: "LKW-Fahrer",
    translation: "truck driver",
    plural: ""
  },
  {
    id: 80,
    genus: masculine,
    noun: "Schriftsteller",
    translation: "writer",
    plural: ""
  },
  {
    id: 81,
    genus: masculine,
    noun: "Redakteur",
    translation: "editor",
    plural: ""
  },
  {
    id: 82,
    genus: masculine,
    noun: "Journalist",
    translation: "journalist",
    plural: ""
  },
  {
    id: 83,
    genus: masculine,
    noun: "Mechaniker",
    translation: "machinist",
    plural: ""
  },
  {
    id: 84,
    genus: masculine,
    noun: "Zimmermann",
    translation: "carpenter",
    plural: ""
  },
  {
    id: 85,
    genus: masculine,
    noun: "Computerprogrammierer",
    translation: "computer programmer",
    plural: ""
  },
  {
    id: 86,
    genus: masculine,
    noun: "persönliche Trainer",
    translation: "personal trainer",
    plural: ""
  },
  {
    id: 87,
    genus: masculine,
    noun: "Vorgesetzte",
    translation: "supervisor",
    plural: ""
  },
  {
    id: 88,
    genus: masculine,
    noun: "Chef",
    translation: "boss",
    plural: ""
  },
  {
    id: 89,
    genus: masculine,
    noun: "Pfleger",
    translation: "caregiver",
    plural: ""
  },
  {
    id: 90,
    genus: masculine,
    noun: "Angestellter",
    translation: "clerk",
    plural: ""
  },
  {
    id: 91,
    genus: masculine,
    noun: "Detektiv",
    translation: "detective",
    plural: ""
  },
  {
    id: 92,
    genus: masculine,
    noun: "Polizeibeamte",
    translation: "police officer",
    plural: ""
  },
  {
    id: 93,
    genus: masculine,
    noun: "Feuerwehrmann",
    translation: "firefighter",
    plural: ""
  },
  {
    id: 94,
    genus: neuter,
    noun: "Frühstuck",
    translation: "breakfast",
    plural: ""
  },
  {
    id: 95,
    genus: neuter,
    noun: "Mittagessen",
    translation: "lunch",
    plural: ""
  },
  {
    id: 96,
    genus: neuter,
    noun: "Abendessen",
    translation: "dinner",
    plural: ""
  },
  {
    id: 97,
    genus: masculine,
    noun: "Snack",
    translation: "snack",
    plural: ""
  },
  {
    id: 98,
    genus: neuter,
    noun: "Dessert",
    translation: "dessert",
    plural: ""
  },
  {
    id: 99,
    genus: masculine,
    noun: "Kuchen",
    translation: "cake",
    plural: ""
  },
  {
    id: 100,
    genus: neuter,
    noun: "Brot",
    translation: "bread",
    plural: ""
  },
  {
    id: 101,
    genus: feminine,
    noun: "Milch",
    translation: "milk",
    plural: ""
  },
  {
    id: 102,
    genus: neuter,
    noun: "Ei",
    translation: "egg",
    plural: ""
  },
  {
    id: 103,
    genus: neuter,
    noun: "Mehl",
    translation: "flour",
    plural: ""
  },
  {
    id: 104,
    genus: masculine,
    noun: "Zucker",
    translation: "sugar",
    plural: ""
  },
  {
    id: 105,
    genus: neuter,
    noun: "Fleisch",
    translation: "meat",
    plural: ""
  },
  {
    id: 106,
    genus: masculine,
    noun: "Truthahn",
    translation: "turkey",
    plural: ""
  },
  {
    id: 107,
    genus: neuter,
    noun: "Schweinefleisch",
    translation: "pork",
    plural: ""
  },
  {
    id: 108,
    genus: neuter,
    noun: "Huhn",
    translation: "chicken",
    plural: ""
  },
  {
    id: 109,
    genus: masculine,
    noun: "Tofu",
    translation: "tofu",
    plural: ""
  },
  {
    id: 110,
    genus: masculine,
    noun: "Salat",
    translation: "salad",
    plural: ""
  },
  {
    id: 111,
    genus: feminine,
    noun: "Pizza",
    translation: "pizza",
    plural: ""
  },
  {
    id: 112,
    genus: feminine,
    noun: "Cracker",
    translation: "cracker",
    plural: ""
  },
  {
    id: 113,
    genus: neuter,
    noun: "Müsli",
    translation: "cereal",
    plural: ""
  },
  {
    id: 114,
    genus: neuter,
    noun: "Haferflocken",
    translation: "oatmeal",
    plural: ""
  },
  {
    id: 115,
    genus: feminine,
    noun: "Pfannkuchen",
    translation: "pancakes",
    plural: ""
  },
  {
    id: 116,
    genus: masculine,
    noun: "Speck",
    translation: "bacon",
    plural: ""
  },
  {
    id: 117,
    genus: feminine,
    noun: "Schokolade",
    translation: "chocolate",
    plural: ""
  },
  {
    id: 118,
    genus: neuter,
    noun: "Obst",
    translation: "fruit",
    plural: ""
  },
  {
    id: 119,
    genus: neuter,
    noun: "Gemüse",
    translation: "vegetable",
    plural: ""
  },
  {
    id: 120,
    genus: masculine,
    noun: "Aprfel",
    translation: "apple",
    plural: ""
  },
  {
    id: 121,
    genus: feminine,
    noun: "Karotte",
    translation: "carrot",
    plural: ""
  },
  {
    id: 122,
    genus: feminine,
    noun: "Birne",
    translation: "pear",
    plural: ""
  },
  {
    id: 123,
    genus: feminine,
    noun: "Banane",
    translation: "banana",
    plural: ""
  },
  {
    id: 124,
    genus: feminine,
    noun: "Tomate",
    translation: "tomato",
    plural: ""
  },
  {
    id: 125,
    genus: feminine,
    noun: "Kartoffel",
    translation: "potato",
    plural: ""
  },
  {
    id: 126,
    genus: masculine,
    noun: "Sellerie",
    translation: "celery",
    plural: ""
  },
  {
    id: 127,
    genus: masculine,
    noun: "Brokkoli",
    translation: "broccoli",
    plural: ""
  },
  {
    id: 128,
    genus: feminine,
    noun: "Zwiebel",
    translation: "onion",
    plural: ""
  },
  {
    id: 129,
    genus: feminine,
    noun: "Gurke",
    translation: "cucumber",
    plural: ""
  },
  {
    id: 130,
    genus: feminine,
    noun: "Zucchini",
    translation: "zucchini",
    plural: ""
  },
  {
    id: 131,
    genus: masculine,
    noun: "Pfirsich",
    translation: "peach",
    plural: ""
  },
  {
    id: 132,
    genus: feminine,
    noun: "Nuss",
    translation: "nut",
    plural: ""
  },
  {
    id: 133,
    genus: masculine,
    noun: "Rosenkohl",
    translation: "brussel sprouts",
    plural: ""
  },
  {
    id: 134,
    genus: feminine,
    noun: "Lasagna",
    translation: "lasagna",
    plural: ""
  },
  {
    id: 135,
    genus: feminine,
    noun: "Spaghetti",
    translation: "spaghetti",
    plural: ""
  },
  {
    id: 136,
    genus: feminine,
    noun: "Makkaroni",
    translation: "macaroni",
    plural: ""
  },
  {
    id: 137,
    genus: feminine,
    noun: "Erdnussbutter",
    translation: "peanut butter",
    plural: ""
  },
  {
    id: 138,
    genus: neuter,
    noun: "Gelee",
    translation: "jelly",
    plural: ""
  },
  {
    id: 139,
    genus: neuter,
    noun: "Sandwich",
    translation: "sandwich",
    plural: ""
  },
  {
    id: 140,
    genus: masculine,
    noun: "Burger",
    translation: "burger",
    plural: ""
  },
  {
    id: 141,
    genus: feminine,
    noun: "Pommes",
    translation: "fries",
    plural: ""
  },
  {
    id: 142,
    genus: feminine,
    noun: "Suppe",
    translation: "soup",
    plural: ""
  },
  {
    id: 143,
    genus: masculine,
    noun: "Fisch",
    translation: "fish",
    plural: ""
  },
  {
    id: 144,
    genus: masculine,
    noun: "Reis",
    translation: "rice",
    plural: ""
  },
  {
    id: 145,
    genus: feminine,
    noun: "Bohnen",
    translation: "beans",
    plural: ""
  },
  {
    id: 146,
    genus: masculine,
    noun: "Burrito",
    translation: "burrito",
    plural: ""
  },
  {
    id: 147,
    genus: masculine,
    noun: "Schinken",
    translation: "ham",
    plural: ""
  },
  {
    id: 148,
    genus: feminine,
    noun: "Pasta",
    translation: "pasta",
    plural: ""
  },
  {
    id: 149,
    genus: masculine,
    noun: "Hund",
    translation: "dog",
    plural: ""
  },
  {
    id: 150,
    genus: feminine,
    noun: "Katze",
    translation: "cat",
    plural: ""
  },
  {
    id: 151,
    genus: masculine,
    noun: "Fisch",
    translation: "fish",
    plural: ""
  },
  {
    id: 152,
    genus: masculine,
    noun: "Vogel",
    translation: "bird",
    plural: ""
  },
  {
    id: 153,
    genus: feminine,
    noun: "Schlange",
    translation: "snake",
    plural: ""
  },
  {
    id: 154,
    genus: feminine,
    noun: "Maus",
    translation: "mouse",
    plural: ""
  },
  {
    id: 155,
    genus: feminine,
    noun: "Rennmaus",
    translation: "gerbil",
    plural: ""
  },
  {
    id: 156,
    genus: masculine,
    noun: "Hamster",
    translation: "hamster",
    plural: ""
  },
  {
    id: 157,
    genus: neuter,
    noun: "Frettchen",
    translation: "ferret",
    plural: ""
  },
  {
    id: 158,
    genus: neuter,
    noun: "Hemd",
    translation: "shirt",
    plural: ""
  },
  {
    id: 159,
    genus: feminine,
    noun: "Hose",
    translation: "pants",
    plural: ""
  },
  {
    id: 160,
    genus: masculine,
    noun: "Mantel",
    translation: "coat",
    plural: ""
  },
  {
    id: 161,
    genus: feminine,
    noun: "Socken",
    translation: "socks",
    plural: ""
  },
  {
    id: 162,
    genus: feminine,
    noun: "Schuhe",
    translation: "shoes",
    plural: ""
  },
  {
    id: 163,
    genus: feminine,
    noun: "Shorts",
    translation: "shorts",
    plural: ""
  },
  {
    id: 164,
    genus: feminine,
    noun: "Unterwäsche",
    translation: "unmasculinewear",
    plural: ""
  },
  {
    id: 165,
    genus: feminine,
    noun: "Bluse",
    translation: "blouse",
    plural: ""
  },
  {
    id: 166,
    genus: masculine,
    noun: "Büstenhalter (BH, for short!)",
    translation: "bra",
    plural: ""
  },
  {
    id: 167,
    genus: feminine,
    noun: "Jeans",
    translation: "jeans",
    plural: ""
  },
  {
    id: 168,
    genus: masculine,
    noun: "Gürtel",
    translation: "belt",
    plural: ""
  },
  {
    id: 169,
    genus: masculine,
    noun: "Hut",
    translation: "hat",
    plural: ""
  },
  {
    id: 170,
    genus: feminine,
    noun: "Krawatte",
    translation: "tie",
    plural: ""
  },
  {
    id: 171,
    genus: neuter,
    noun: "Kleid",
    translation: "dress",
    plural: ""
  },
  {
    id: 172,
    genus: masculine,
    noun: "Rock",
    translation: "skirt",
    plural: ""
  },
  {
    id: 173,
    genus: feminine,
    noun: "Stiefel",
    translation: "boots",
    plural: ""
  },
  {
    id: 174,
    genus: masculine,
    noun: "Baseball",
    translation: "baseball",
    plural: ""
  },
  {
    id: 175,
    genus: masculine,
    noun: "Basketball",
    translation: "basketball",
    plural: ""
  },
  {
    id: 176,
    genus: masculine,
    noun: "Fuβball",
    translation: "soccer",
    plural: ""
  },
  {
    id: 177,
    genus: masculine,
    noun: "Football",
    translation: "football",
    plural: ""
  },
  {
    id: 178,
    genus: neuter,
    noun: "Hockey",
    translation: "hockey",
    plural: ""
  },
  {
    id: 179,
    genus: neuter,
    noun: "Rugby",
    translation: "rugby",
    plural: ""
  },
  {
    id: 180,
    genus: neuter,
    noun: "Tennis",
    translation: "tennis",
    plural: ""
  },
  {
    id: 181,
    genus: neuter,
    noun: "Lacrosse",
    translation: "lacrosse",
    plural: ""
  },
  {
    id: 182,
    genus: neuter,
    noun: "Cricket",
    translation: "cricket",
    plural: ""
  },
  {
    id: 183,
    genus: masculine,
    noun: "Volleyball",
    translation: "volleyball",
    plural: ""
  },
  {
    id: 184,
    genus: masculine,
    noun: "Golf",
    translation: "golf",
    plural: ""
  },
  {
    id: 185,
    genus: masculine,
    noun: "Geldbeutel",
    translation: "purse",
    plural: ""
  },
  {
    id: 186,
    genus: neuter,
    noun: "Handy",
    translation: "cellphone",
    plural: ""
  },
  {
    id: 187,
    genus: feminine,
    noun: "Schlüssel",
    translation: "keys",
    plural: ""
  },
  {
    id: 188,
    genus: neuter,
    noun: "Portemonnaie",
    translation: "wallet",
    plural: ""
  },
  {
    id: 189,
    genus: neuter,
    noun: "Geld",
    translation: "money",
    plural: ""
  },
  {
    id: 190,
    genus: feminine,
    noun: "Uhr",
    translation: "watch",
    plural: ""
  },
  {
    id: 191,
    genus: masculine,
    noun: "Schmuck",
    translation: "jewelry",
    plural: ""
  },
  {
    id: 192,
    genus: masculine,
    noun: "Lippenstift",
    translation: "chapstick",
    plural: ""
  },
  {
    id: 193,
    genus: feminine,
    noun: "Tasche",
    translation: "bag",
    plural: ""
  },
  {
    id: 194,
    genus: masculine,
    noun: "Laptop",
    translation: "laptop",
    plural: ""
  },
  {
    id: 195,
    genus: masculine,
    noun: "iPod",
    translation: "iPod",
    plural: ""
  },
  {
    id: 196,
    genus: masculine,
    noun: "MP3-Player",
    translation: "MP3 player",
    plural: ""
  },
  {
    id: 197,
    genus: neuter,
    noun: "Notebook",
    translation: "notebook",
    plural: ""
  },
  {
    id: 198,
    genus: feminine,
    noun: "Kreditkarte",
    translation: "credit card",
    plural: ""
  },
  {
    id: 199,
    genus: masculine,
    noun: "Führerschein",
    translation: "driver’s license",
    plural: ""
  }
]

export default nounList
