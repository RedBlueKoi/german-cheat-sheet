import verbs1 from "./verbs1.mock"
import IVerb from "~/types/IVerb"

const verbsMock: IVerb[] = [...verbs1]

export default verbsMock
