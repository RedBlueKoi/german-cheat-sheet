import IVerb from "~/types/IVerb"

const irregularDividableVerbs: IVerb[] = [
  {
    id: 0,
    verb: "abfahren",
    verbDisplay: "ab * fahren",
    present: "fährt ab",
    past: "furt ab",
    perfect: "ist abgefahren",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 2,
    verb: "abfliegen",
    verbDisplay: "abfliegen",
    present: "fliegt ab",
    past: "flog ab",
    perfect: "ist abgeflogen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 3,
    verb: "abgeben",
    verbDisplay: "abgeben",
    present: "gibt ab",
    past: "gab ab",
    perfect: "hat abgegeben",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 4,
    verb: "abschließen",
    verbDisplay: "abschließen",
    present: "schließt ab",
    past: "schloss ab",
    perfect: "hat abgeschlossen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 5,
    verb: "anbieten",
    verbDisplay: "anbieten",
    present: "bietet an",
    past: "bot an",
    perfect: "hat angeboten",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 6,
    verb: "anfangen",
    verbDisplay: "anfangen",
    present: "fängt an",
    past: "fing an",
    perfect: "hat angefangen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 7,
    verb: "ankommen",
    verbDisplay: "ankommen",
    present: "kommt an",
    past: "kam an",
    perfect: "ist angekommen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 8,
    verb: "anrufen",
    verbDisplay: "anrufen",
    present: "ruft an",
    past: "rief an",
    perfect: "hat angerufen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 9,
    verb: "ansehen",
    verbDisplay: "ansehen",
    present: "sieht an",
    past: "sah an",
    perfect: "hat angesehen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  },
  {
    id: 10,
    verb: "anziehen",
    verbDisplay: "anziehen",
    present: "ziehen an",
    past: "zog an",
    perfect: "hat angezogen",
    proposition: undefined,
    isRegular: false,
    isDividable: true,
    translation: ""
  }
]

export default irregularDividableVerbs
