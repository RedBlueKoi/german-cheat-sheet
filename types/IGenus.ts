export default interface IGenus {
  name: string
  article: string
  translation: string
}
