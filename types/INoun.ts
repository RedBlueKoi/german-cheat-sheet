import IGenus from "~/types/IGenus"

export default interface INoun {
  id: number
  noun: string
  genus: IGenus
  translation: string
  plural: string
}
