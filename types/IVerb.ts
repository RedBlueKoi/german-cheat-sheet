import IProposition from "~/types/IProposition"

export default interface IVerb {
  id: number
  verb: string
  verbDisplay: string
  present: string
  past: string
  perfect: string
  proposition?: IProposition
  isRegular: boolean
  isDividable: boolean
  translation: string
}
