import INoun from "~/types/INoun"

export default interface IGenusGameNoun extends INoun {
  guessed: boolean
  correct: boolean
}
