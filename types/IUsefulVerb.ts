import ECase from "~/types/ECase"

export default interface IUsefulVerb {
  id: number
  verb: string
  proposition: string
  case: ECase
  translation: string
  [key: string]: any
}
