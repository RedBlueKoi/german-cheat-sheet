import ECase from "~/types/ECase"

export default interface IProposition {
  name: string
  caseAfter: ECase[]
}
