export default interface IIrregularVerb {
  id: number
  infinitive: string
  present: string
  past: string
  perfect: string
  translation: string
  [key: string]: any
}
