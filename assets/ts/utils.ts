interface IGeneric {
  [key: string]: string
}

const getFilteredWords = (wordsArray: any[], filter: string): any[] => {
  const processedFilter: string = filter.trim().toLowerCase()
  // If filter is empty
  if (processedFilter === "") {
    return wordsArray
  }

  return wordsArray.filter((item: IGeneric) => {
    for (const property in item) {
      // Simplify the prop value call
      const propValue: string = item[property].trim().toLowerCase()
      // Return the prop if it's matching the filter
      if (propValue.includes(processedFilter)) return true
    }
    // Return if the filter doesn't match
    return false
  })
}

const getRandFrom = (amount: number): number => {
  return Math.floor(Math.random() * amount)
}

export default {
  getFilteredWords,
  getRandFrom
}
